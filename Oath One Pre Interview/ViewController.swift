//
//  ViewController.swift
//  Oath One Pre Interview
//
//  Created by Truptika on 06/04/22.
//

import UIKit
import PDFKit

class ViewController: UIViewController {
    
    @IBOutlet weak var pdfView: PDFView!
    @IBOutlet weak var thumbnailViewContainer: UIView!
    @IBOutlet var thumbnailView: PDFThumbnailView!
    @IBOutlet var colorButtonCollection: [UIButton]!
    @IBOutlet var toolButtonCollection: [UIButton]!
    
    private let pdfDrawer = PDFDrawer()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupPDFView()
        
        let pdfDrawingGestureRecognizer = DrawingGestureRecognizer()
        pdfView.addGestureRecognizer(pdfDrawingGestureRecognizer)
        pdfDrawingGestureRecognizer.drawingDelegate = pdfDrawer
        pdfDrawer.pdfView = pdfView
        
        guard let path = Bundle.main.url(forResource: "Oath_PDF", withExtension: "pdf") else { return }
        self.pdfView.document = PDFDocument(url: path)
    }
    
    func setupPDFView() {
        self.pdfView.displayDirection = .vertical
        self.pdfView.usePageViewController(true)
        self.pdfView.pageBreakMargins = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        self.pdfView.autoScales = true
        self.pdfView.backgroundColor = view.backgroundColor ?? UIColor(white: 0.95, alpha: 1.0)
        
        self.thumbnailView.pdfView = self.pdfView
        self.thumbnailView.thumbnailSize = CGSize(width: 100, height: 100)
        self.thumbnailView.layoutMode = .vertical
        self.thumbnailView.backgroundColor = self.thumbnailViewContainer.backgroundColor
        
        self.colorButtonCollection.forEach { button in
            button.layer.cornerRadius = button.bounds.width / 2
            button.layer.masksToBounds = true
            if button.tag == 201 {
                button.layer.borderWidth = 3.0
                button.layer.borderColor = UIColor.white.cgColor
            }
        }
        
        self.toolButtonCollection.forEach { button in
            if button.tag == 102 {
                button.tintColor = .red
            } else {
                button.tintColor = .black
            }
        }
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        // This call is required to fix PDF document scale, seems to be bug inside PDFKit
        pdfView.autoScales = true
    }

    @IBAction func clickOnColorButtton(_ sender: UIButton) {
        if sender.tag == 201 { //red
            pdfDrawer.color = .red
        } else if sender.tag == 202 { //blue
            pdfDrawer.color = .blue
        } else if sender.tag == 203 { // black
            pdfDrawer.color = .black
        }
        
        self.colorButtonCollection.forEach { button in
            if button.tag == sender.tag {
                button.layer.borderWidth = 3.0
                button.layer.borderColor = UIColor.white.cgColor
            } else {
                button.layer.borderWidth = 0.0
                button.layer.borderColor = UIColor.clear.cgColor
            }
        }
    }
    
    @IBAction func clickOnDrawingToolButtton(_ sender: UIButton) {
        self.toolButtonCollection.forEach { button in
            if button.tag == sender.tag {
                button.tintColor = .red
            } else {
                button.tintColor = .black
            }
        }
        
        if sender.tag == 101 {
            pdfDrawer.drawingTool = .eraser
        } else if sender.tag == 102 {
            pdfDrawer.drawingTool = .pencil
        } else if sender.tag == 103 {
            pdfDrawer.drawingTool = .pen
        } else if sender.tag == 104 {
            pdfDrawer.drawingTool = .highlighter
        } else if sender.tag == 105 {
            self.sharePdf(sender)
        }
    }
    
    func sharePdf(_ sender: UIButton) {
        if let pdfData = pdfView.document?.dataRepresentation() {
            let activityViewController: UIActivityViewController = UIActivityViewController(activityItems: [pdfData], applicationActivities: nil)
            self.present(activityViewController, animated: true, completion: nil)
            if let popOver = activityViewController.popoverPresentationController {
              popOver.sourceView = sender
            }
        }
    }
}

